﻿namespace Virtues.UI
{
    using UnityEngine;
    using System;
    [CreateAssetMenu(fileName = "DialogueBox Settings", menuName = "PaladinEngine/Settings/New DialogueBox Settings", order = 1)]
    public class DialogueWindowSettings : ScriptableObject
    {
        [Header("Font")]
        public Font font;
        [Header("SFX")]
        public AudioClip letterSFX;
        public AudioClip cursorSFX;
        public AudioClip submitSFX;
        public AudioClip cancelSFX;
        public AudioClip nextDialogueSFX;
        public AudioClip endDialogueSFX;
        [Header("Graphics")]
        public Sprite windowSkinSprite;
        public Sprite commandCursorSprite;
    }
}


