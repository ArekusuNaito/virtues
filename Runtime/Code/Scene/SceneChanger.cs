﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UniRx.Async;
using DG.Tweening;
// using CoroutineAsyncExtensions;
namespace  Virtues.Scene
{
    [CreateAssetMenu(fileName = "New Scene Changer", menuName = "Virtues/Scene/Scene Changer")]
    public class SceneChanger : ScriptableObject
    {
        public async UniTask<string> ChangeScene(int sceneIndex, Action<float> onProgress=null)
        {
            // await SceneManager.LoadSceneAsync(sceneIndex);
            // DOTween.Clear(); //Clean every tweening before changing scenes. But logs a warning
            await SceneManager.LoadSceneAsync(sceneIndex).ConfigureAwait(new Progress<float>(currentProgress =>
            {
                if(onProgress!=null)
                {
                    onProgress(currentProgress);
                }
            }));
            
            return null;
        }
    }

}