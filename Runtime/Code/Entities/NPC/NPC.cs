﻿namespace Virtues.Entities
{
	using UnityEngine;

	[RequireComponent(typeof(SpriteRenderer))]
	[RequireComponent(typeof(BoxCollider2D))]
	public class NPC : Entity
	{	
		[TextArea]
		public string[] dialogues;
	}
}