﻿namespace Virtues.Entities
{
	using UnityEngine;

	[RequireComponent(typeof(SpriteRenderer))]
	[RequireComponent(typeof(BoxCollider2D))]
	public abstract class Entity : MonoBehaviour
	{
		
		protected SpriteRenderer spriteRenderer;
		protected BoxCollider2D boxCollider;

		protected virtual void Start()
		{
			this.GetComponents();
			this.InitializeComponents();
		}

		protected virtual void GetComponents()
		{
			this.spriteRenderer = GetComponent<SpriteRenderer>();
			this.boxCollider = GetComponent<BoxCollider2D>();
		}

		protected virtual void InitializeComponents()
		{
			
		}

	}
}