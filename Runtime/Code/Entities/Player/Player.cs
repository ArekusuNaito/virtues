﻿namespace Virtues.Entities
{
	using UniRx;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using Input;
	using System;
	

	[RequireComponent(typeof(Rigidbody2D))]
	public class Player : Entity 
	{
		[Header("Movement")]
		public float walkVelocity;
		public Facing facing = Facing.Down;

		[Header("Collision")]
		ContactFilter2D contactFilter;
		float collisionShellRadius;

		//
		Rigidbody2D body;
		
		Vector2 deltaDistance = Vector2.zero;
		private bool isPaused=false;

		public enum Facing
		{
			Up,Down,Left,Right
		}
		
		#region Custom Actions that need configuration from outside
		public Action onPause;
		public Action onUnPause;
		public Action<GameObject> OnInteract;
		#endregion



		protected override void GetComponents()
		{
			base.GetComponents();
			this.body = GetComponent<Rigidbody2D>();
		}

		protected override void InitializeComponents() 
		{
			base.InitializeComponents();
			InitializeRigidBody();
			InitializeContactFilter();
			collisionShellRadius = boxCollider.size.x / 1000f;
		}

		void InitializeContactFilter()
		{
			contactFilter.useTriggers = false;
			contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(this.gameObject.layer));
			contactFilter.useLayerMask = true;
		}

		void InitializeRigidBody()
		{
			this.body.bodyType = RigidbodyType2D.Kinematic;
			this.body.simulated = true;
			
		}

		public bool IsPaused
		{
			get{return isPaused;}
		}

		public void Pause()
		{
			this.isPaused=true;
			onPause();
			
		}

		public void UnPause()
		{
			this.isPaused=false;
			onUnPause();
		}

		public void Action()
		{
			Debug.Log("Player.Action()");
			var direction = new Vector2(0,1);
			var hitResults = new RaycastHit2D[8];
			var hitCount = this.body.Cast(direction,hitResults,1);
			for(var index=0;index<hitCount;index++)
			{
				var hit = hitResults[index];
				if(hit)
				{
					Debug.Log($"You hit {hit.transform.name}");
					OnInteract(hit.transform.gameObject);
				}
			}			
		}

		void TalkTo(NPC npc)
		{
			this.Pause();
			// PaladinEngine.UI.HUD.ShowDialogueBox(npc.dialogues);

		}

		void Movement(ref float deltaAxis,float sign)
		{
			ResetDeltaDistance();
			// this.deltaDistance.y = sign*GetDistanceInOneFrame(walkVelocity);
			deltaAxis = sign*GetDistanceInOneFrame(walkVelocity);
			LookForCollisions(ref deltaDistance);
			MoveRigidBody(deltaDistance);
		}


		float GetDistanceInOneFrame(float velocity)
		{
			return velocity * Time.deltaTime;
		}

		

		void ResetDeltaDistance()
		{
			this.deltaDistance = Vector2.zero;
		}

		void LookForCollisions(ref Vector2 movePosition)
		{
			// Debug.Log($"Moving to: {movePosition}");
			if(movePosition.magnitude!=0)
			{
				RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
				int collisionCount = this.body.Cast(movePosition,this.contactFilter,hitBuffer,movePosition.magnitude + collisionShellRadius);
				
				//Check collisions in every raycastHit2D				
				for(int index=0;index<collisionCount;index++)
				{
					var collision = hitBuffer[index];
					//Checking the normal tells me to what direction the player is touching
					if(collision.normal.y!=0) //Collision vertical?
					{
						movePosition.y = 0;
					}
					else if(collision.normal.x!=0)
					{
						movePosition.x=0;
					}	
				}
			}	
		}


		public void Move(Vector2 direction)
		{
			// print($"Move is being called");
			if(!isPaused)
			{
				this.deltaDistance.x = direction.x * GetDistanceInOneFrame(walkVelocity);
				this.deltaDistance.y = direction.y * GetDistanceInOneFrame(walkVelocity);
				LookForCollisions(ref deltaDistance);
				MoveRigidBody(deltaDistance);
			}
			
		}

		void MoveRigidBody(Vector2 movePosition)
		{
			this.body.position+= movePosition;
		}
	}

}