﻿namespace Virtues.Audio
{   
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "Audio Collection", menuName = "Virtues/Audio/Audio Collection")]
    public class AudioCollection : ScriptableObject
    {
        [SerializeField]List<AudioClip> files;

        public int Count
        {
            get { return this.files.Count; }
        }
        
        public AudioClip this[int index]
        {
            get { return this.files[index]; }
            set { this.files[index] = value; }
        }
    }

}