﻿namespace Virtues.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "Audio Player", menuName = "Virtues/Audio/Audio Player")]
    public class AudioPlayer : ScriptableObject
    {
        // Start is called before the first frame update
        [HideInInspector]public AudioSource musicChannel;
        [HideInInspector]public AudioSource sfxChannel;
        public UnityEngine.Audio.AudioMixerGroup musicGroup;
        public UnityEngine.Audio.AudioMixerGroup sfxGroup;

        void OnEnable()
        {
            
        }

        public void Initialize()
        {
            // Debug.Log("Music Player On Enable");
            // var musicObject = GameObject.FindWithTag(musicChannelTag);
            // var sfxObject = GameObject.FindWithTag(sfxChannelTag);
            // if(musicChannel)
            // {
            //     this.musicChannel = musicObject.GetComponent<AudioSource>();
            // }
            // if(sfxChannel)
            // {
            //     this.sfxChannel = sfxObject.GetComponent<AudioSource>();
            // }else Debug.Log("No hay, no existe");
            var audioSources = GameObject.FindObjectsOfType<AudioSource>();
            foreach(var audioSource in audioSources)
            {
                if(audioSource.outputAudioMixerGroup==musicGroup)this.musicChannel=audioSource;
                if(audioSource.outputAudioMixerGroup==sfxGroup)this.sfxChannel=audioSource;
            }
        }

        public void PlayMusic(AudioClip songClip)
        {
            this.musicChannel.clip = songClip;
            this.musicChannel.Play();
        }

        public void PlaySFX(AudioClip sfxClip)
        {
            this.sfxChannel.PlayOneShot(sfxClip);
        }
    }

}