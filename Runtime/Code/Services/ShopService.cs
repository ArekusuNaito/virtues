﻿namespace Virtues.Services
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UniRx;
    using Models;

    public class ShopService : MonoBehaviour
    {
        public static bool CanItBeAfforded(IntReactiveProperty currency,Buyable buyable,int quantityToBuy=1)
        {
            return currency.Value>=buyable.Price.Value* quantityToBuy;
        }

        #region Buy
        //###########################################################################################################################
        // Buy functions
        //###########################################################################################################################
        public static void Buy(Buyable buyable,IntReactiveProperty currency,Inventory inventory, int quantityToBuy=1)
        {
            int totalPrice = buyable.Price.Value*quantityToBuy;
            if(CanItBeAfforded(currency,buyable,quantityToBuy))
            {
                CurrencyService.Substract(currency,totalPrice);
                inventory.Add(buyable as Item,quantityToBuy);
            }
            else Debug.Log($"You can't afford that");
            
        }
        #endregion
        #region Sell
        //###########################################################################################################################
        // Sell functions
        //###########################################################################################################################

        public static void Sell(Priceable sellable,IntReactiveProperty currency,Inventory inventory, int quantityToSell=1)
        {
            inventory.Remove(sellable as Item, quantityToSell);
            CurrencyService.Add(currency,sellable.Price.Value * quantityToSell);
        }

        public static void Sell(Priceable sellable,IntReactiveProperty currency,System.Action<Item,int> removeItem,int quantityToSell=1)
        {
            removeItem(sellable as Item,quantityToSell);
            CurrencyService.Add(currency,sellable.Price.Value*quantityToSell);
            
        }

        #endregion
    }

}