﻿namespace Virtues.Services
{
    using UnityEngine;
    using UniRx;
    using System;
    public static class BehaviourService
    {
        public static IDisposable AddFollowMovement(GameObject gameObjectToUse, Transform targetToFollow, float velocity = 1)
        {
            var observable = Observable.EveryUpdate().Subscribe(_ =>
            {
                if(targetToFollow==null)return;
                var targetPosition = targetToFollow.position;
                var direction = (targetPosition - gameObjectToUse.transform.position).normalized;
                direction*=velocity;
                Debug.Log(direction);
                gameObjectToUse.transform.Translate(PhysicsService.DistanceInOneFrame(direction));
            });
            observable.AddTo(gameObjectToUse);
            return observable;
        }

        
        
        public static void AddHopperMovement(GameObject gameObjectToUse,float speedFactor=5)
        {
            //TODO: Add ability to customize the intervals/timer
            // CardinalPoints.GetRandomDirection().vector * speedFactor;
            var velocity = Vector2.zero;
            var interval1 = Observable.Interval(System.TimeSpan.FromSeconds(2.5f));
            var interval2 = Observable.Timer(TimeSpan.FromSeconds(0.3f));
            var many = Observable.SelectMany(interval1, interval2).Subscribe(_ =>
             {
                 velocity = Vector2.zero; //stop it every 0.3f , given by the timer
             }).AddTo(gameObjectToUse);

            interval1.Subscribe(intervalCount =>
            {
                //each of these intervals it will recover the speed
                velocity = CardinalPoints.GetRandom4Direction().vector * speedFactor;
            }).AddTo(gameObjectToUse);


            Observable.EveryUpdate().Subscribe(_ =>
            {
                gameObjectToUse.transform.Translate(PhysicsService.DistanceInOneFrame(velocity));
            }).AddTo(gameObjectToUse);

        }

        public static void AddShooter()
        {
            // Observable.Interval(System.TimeSpan.FromSeconds(bulletInterval)).Subscribe(_ =>
            // {
            //     var cardinalDirection = CardinalPoints.GetRandomDirection();
            //     var position = CardinalPoints.DirectionToBoundsPosition(cardinalDirection,this.boxCollider.bounds);
            //     var bulletClone = Instantiate(bullet,position,Quaternion.identity).GetComponent<Projectile>();
            //     bulletClone.Initialize(this.facing);
            // }).AddTo(this);
        }


    }
}