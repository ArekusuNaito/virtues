﻿namespace Virtues.Services
{
    
    using UnityEngine;

    public static class PhysicsService 
    {
        public static float ApplyGravity(Vector2 velocity,float gravity)
        {
            //Gravity is a negative value, don't forget!
            return velocity.y += gravity * Time.deltaTime;
        }

        /// <summary>
        /// Input is velocity current velocity
        /// </summary>
        /// <param name="velocity"></param>
        /// <returns>Returns: DISTANCE traveled in one  frame (Not velocity!)</returns>
        public static Vector2 DistanceInOneFrame(Vector2 velocity)
        {
            return velocity*Time.deltaTime;
        }
        
    }

    

}