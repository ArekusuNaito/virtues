﻿namespace Virtues.Services
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Models;
    using System.Linq;

    public class BakeryService 
    {
        public static void Create(Recipe recipe, Inventory inventory)
        {   
            //Do you meet the requirements?
            
            Debug.Log($"Can you create the recipe? {RequirementsAreMet(recipe,inventory)}");
            if(RequirementsAreMet(recipe,inventory))
            {
                inventory.LogInConsole();
                recipe.requirements.ForEach(requirement=>inventory.Remove(requirement.item,requirement.quantity));
                recipe.awards.ForEach(award=>inventory.Add(award.item,award.quantity));
                inventory.LogInConsole();
            }
            else
            {
                Debug.Log($"Inventory doesn't meet the requirements to create {recipe.name}");
            }
        }

        static bool RequirementsAreMet(Recipe recipe,Inventory inventory)
        {
            Debug.Log(inventory.IsEmpty);
            if(inventory.IsEmpty)return false;
            //otherwise
            return recipe.requirements.All<RecipeRequirement>(requirement => inventory.Has(requirement.item, requirement.quantity));
            
        }

        public static bool CanItBeCreated(Recipe recipe, Inventory inventory)
        {
            return RequirementsAreMet(recipe,inventory);
        }

        public static int HowManyCanYouMake(Recipe recipe, Inventory inventory)
        {
            if(CanItBeCreated(recipe,inventory))
            {
                return recipe.requirements
                //For every requirement, tell me how many can I make per requiredItem
                .Select(requirement=>inventory[requirement.item].quantity.Value/requirement.quantity)
                .Min(); 
            }
            else return 0;

        }

    }

}