﻿namespace Virtues.Services
{
    using UnityEngine;
    using System.Collections.Generic;
    using System.Linq;
    public static class CollisionService
    {
        private const float shellOutline = 0.01f;

        /// <summary>
        /// After a successful collison was detected. You can send the raycasthits, your collider and the distance in that frame
        /// to correct the position of the player to make it look like it never went inside the collider.
        /// </summary>
        /// <param name="collider"></param>
        /// <param name="hits"></param>
        /// <param name="distanceInOneFrame"></param>
        public static void RepositionCollider(Collider2D collider,List<RaycastHit2D> hits,ref Vector2 distanceInOneFrame)
        {
            if(hits==null)return;
            float hitDistance=0;
            var deltaDistance = distanceInOneFrame;
            var directionX = Mathf.Sign(distanceInOneFrame.x);
            var directionY = Mathf.Sign(distanceInOneFrame.y);
            foreach (var hit in hits)
            {
                
                hitDistance = hit.distance - shellOutline;
                var degreesAngle = Vector2.Angle(hit.normal, distanceInOneFrame);
                // if(degreesAngle >0 && degreesAngle<180)
                // {
                //     // Debug.Log(degreesAngle);
                //     // var radiansAngle = degreesAngle*Mathf.Deg2Rad;
                //     // //
                //     // var x = Mathf.Cos(radiansAngle) * hitDistance;
                //     // x = Mathf.Abs(x)*Mathf.Sign(directionX);
                //     // //
                //     // var y = Mathf.Sin(radiansAngle) * hitDistance;
                //     // y = Mathf.Abs(y) * Mathf.Sign(directionY);
                //     // //
                //     // collider.transform.Translate(new Vector2(x,y));
                //     // Debug.Log($"X:{x}, Y:{y}, hitDistance:{hitDistance}");
                // }
                // else
                // {
                //     distanceInOneFrame = distanceInOneFrame.normalized * hitDistance;
                // }
                distanceInOneFrame = distanceInOneFrame.normalized * hitDistance;
                break;
            }
            
        }

        /// <summary>
        /// Detects if the given collider collided with anything the filter describes.
        /// We need the distance in that frame to calculate any hits.
        /// </summary>
        /// <param name="collider"></param>
        /// <param name="filter"></param>
        /// <param name="distanceToMoveInOneFrame"></param>
        /// <returns>A list of RaycastHits2D that the given collider collided with</returns>
        public static List<RaycastHit2D> Detect(Collider2D collider, ContactFilter2D filter,Vector2 distanceToMoveInOneFrame)
        {
            var hitBuffer = new RaycastHit2D[8];
            var rayDistance = distanceToMoveInOneFrame.magnitude + shellOutline;
            
            var collisionCount = collider.Cast(distanceToMoveInOneFrame,filter, hitBuffer, rayDistance);            
            if (collisionCount > 0)
            {
                var hitList = new List<RaycastHit2D>(hitBuffer); //we get 8 spaces even if they are null
                hitList.RemoveRange(collisionCount,hitList.Count-collisionCount); //with remove range we get the actual existent hits, removing the nulls
                return hitList;
            }
            else return new List<RaycastHit2D>(); //an empty list
        }

        
        
    }
}