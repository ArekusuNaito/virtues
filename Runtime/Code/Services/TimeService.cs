﻿namespace Virtues.Services
{
    using UniRx;
    using System;
    using UnityEngine;

    public class TimeService
    {
        /// <summary>
        /// Do you need something to happen after x time ? Use this!
        /// </summary>
        /// <param name="duration">Timer duration, after this time the action will be executed</param>
        /// <param name="action"></param>
        public static IDisposable Timer(TimeSpan duration, Action action)
        {
            return Observable.Timer(duration)
            .Subscribe((_) =>
            {
                action();
            });

        }


        public static IObservable<float> EveryElapsedSeconds()
        {
            var stream = Observable.EveryUpdate().TimeInterval()
                .Select(timeInterval => (float)timeInterval.Interval.TotalSeconds)
                .Scan((previousTime, currentTime) =>
                {
                    return previousTime + currentTime;
                });
                return stream;
                
                // .Where(elapsedTime => elapsedTime < bakeTime);

        }

        public static IObservable<float> EveryDeltaTime()
        {
            return Observable.EveryUpdate().TimeInterval()
            .Select(timeInterval => (float)timeInterval.Interval.TotalSeconds);
        }

        
        
    }
}