﻿namespace Virtues.Services
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UniRx;
    public class CurrencyService : MonoBehaviour
    {
        public static void Add(IntReactiveProperty currency,int quantityToAdd)
        {
            currency.Value+=quantityToAdd;
        }

        public static void Substract(IntReactiveProperty currency,int quantityToSubstract)
        {
            currency.Value= currency.Value-quantityToSubstract < 0? 0 :currency.Value - quantityToSubstract;

        }
        
    }

}