﻿namespace Virtues.Services
{
    using UnityEngine;
    using UniRx;
    using System;
    public static class CameraService
    {
        public static void Shake(float shakeTime,float power = 0.4f,Camera camera = null)
        {
            if(camera== null)camera=Camera.main;
            var originalPosition = camera.transform.localPosition;
            var timer = Observable.Timer(TimeSpan.FromSeconds(shakeTime));
            Observable.EveryUpdate().TakeUntil(timer).Subscribe(_=>
            {
                camera.transform.localPosition = originalPosition+ UnityEngine.Random.insideUnitSphere * power;
            },()=>
            {
                camera.transform.localPosition = originalPosition;
            }).AddTo(Camera.main);

        }
    }
}