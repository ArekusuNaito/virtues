namespace Virtues.Services
{
    
    using System.Collections.Generic;
    using UnityEngine;
    using System;
    using Services;
    using UniRx;
    using Models;
    using System.Linq;

    
    public class Inventory : MonoBehaviour
    {
        
        public List<Item> items = new List<Item>();
        

        public void Add(Item itemToAdd, int totalQuantityToAdd=1)
        {

            //If the item doesn't exist create a new one with quantity=0
            //The algorithm will always work with an existing item. And with a new item, we can work with an existing item with quantity 0
            
            while(totalQuantityToAdd>0)
            {
                
                var itemInInventory = this.GetNonStackedItem(itemToAdd);
                if(!itemInInventory)itemInInventory = AddNewItem(itemToAdd,0);
                // Debug.Log(this.GetTotalQuantityOf(itemInInventory));
                int HowMuchMoreCanYouStack = itemInInventory.HowMuchMoreCanYouStack();
                int quantityToAddToThisStack = (totalQuantityToAdd > HowMuchMoreCanYouStack) ? HowMuchMoreCanYouStack : totalQuantityToAdd;
                itemInInventory.quantity.Value+=quantityToAddToThisStack;
                totalQuantityToAdd-=quantityToAddToThisStack;
                // Debug.Log(this.GetTotalQuantityOf(itemInInventory));
                // Debug.Log($"#####");

            }
            
        }

        public int GetTotalQuantityOf(Item itemNeeded)
        {
            return this.items.Sum((currentItem)=>
            {
                return currentItem.quantity.Value;
            });
        }


        /// <summary>
        /// No questions asked, you "force" an item to the inventory with the quantity you want
        /// </summary>
        /// <param name="itemToAdd"></param>
        /// <param name="initialQuantity"></param>
        Item AddNewItem(Item itemToAdd, int initialQuantity = 1)
        {
            // By cloning the scriptable object you don't modify the object in editor mode
            // When quantity gets modified it messes with a lot of stuff
            // Cloning changes the name to the original, instead of the "clone" name Unity names automatically
            var clonedItemToAdd = Kernel.CloneScriptableObject<Item>(itemToAdd);
            clonedItemToAdd.quantity.Value = initialQuantity;
            items.Add(clonedItemToAdd);
            return clonedItemToAdd;
        }


        public Item Get(Item itemNeeded)
        {
            return items.Find(currentItem => currentItem.name == itemNeeded.name);
        }


        /// <summary>
        /// Get the the next item that you can stack more items to it
        /// </summary>
        /// <param name="itemNeeded"></param>
        /// <returns></returns>
        public Item GetNonStackedItem(Item itemNeeded)
        {
            
            return items.Find(currentItem => currentItem.name == itemNeeded.name && currentItem.quantity.Value<currentItem.StackSize.Value);
        }

        public Item this[Item itemNeeded]
        {
            get
            {
                return Get(itemNeeded);
            }
        }

        public Item this[int index]
        {
            get
            {
                return this.items[index];
            }
        }


        public void Remove(Item itemThatWillBeRemoved,int totalQuantityToRemove)
        {
            
            while(totalQuantityToRemove>0)
            {
                var itemInInventory = this.Get(itemThatWillBeRemoved);
                if(!itemInInventory)return;
                // if(!itemInInventory)return;
                //Eg. If stacksize =5, you can take a maximum of 5, if you want to take 6, you have to modify 2 different items.
                //One with 5 and one with 1.
                int quantityToRemoveToThisStack = (totalQuantityToRemove > itemInInventory.StackSize.Value) ? itemInInventory.StackSize.Value : (totalQuantityToRemove);
                itemInInventory.quantity.Value -= quantityToRemoveToThisStack;
                if(itemInInventory.quantity.Value<=0)
                {
                    //If the item quantity has negative value we have to take in consideration this extra value
                    //To not substract it to the totalQuantity, since it will affect the calcuation for other stacks
                    totalQuantityToRemove +=Mathf.Abs(itemInInventory.quantity.Value); 
                    this.items.Remove(itemInInventory);
                }
                totalQuantityToRemove-=quantityToRemoveToThisStack;

            }
        }

        public void LogInConsole()
        {
            Debug.Log($"Items in Bag");
            items.ToList().ForEach(item=> Debug.Log($"{item.name}x{item.quantity}"));
            Debug.Log($"########");
        }

        public bool Has(Item itemNeeded,int quantityNeeded)
        { 
            var itemInInventory = this.Get(itemNeeded);
            if(itemInInventory)
            {
                return this.GetTotalQuantityOf(itemInInventory)>= quantityNeeded ? true : false;
            }
            else return  false;
            
            
        }

        public void Awake()
        {
            this.items.ForEach(item=>Debug.Log(item.GetInstanceID()));
            //Transform all scriptable objects in a list, to a clone, this way it won't be modified
            this.items = this.items.Select(item=>Kernel.CloneScriptableObject<Item>(item)).ToList();
            this.items.ForEach(item=>Debug.Log(item.GetInstanceID()));
        }

        public bool IsEmpty
        {
            get{return this.items.Count==0;}
        }

        public int Count
        {
            get{return this.items.Count;}
        }
    }
}
