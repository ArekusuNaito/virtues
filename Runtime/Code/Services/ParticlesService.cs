﻿namespace Virtues.Services
{

    using UnityEngine;

    public static class ParticlesService
    {
        public static ParticleSystem Create(ParticleSystem particles, Transform transform)
        {
            //clone the given particles
            var clonedParticles = Object.Instantiate(particles, transform.position, Quaternion.identity);
            Object.Destroy(clonedParticles.gameObject, clonedParticles.main.duration);
            return clonedParticles;

        }

    }



}