
namespace Virtues.Services
{
    using UniRx;
    using System;
    using UnityEngine;
    public class BattleService
    {
        public static void Attack(float damage, ref float whatIsGettingHurt)
        {
            whatIsGettingHurt -= damage;
        }

        public static void Attack(float damage, FloatReactiveProperty whatIsGettingHurt)
        {
            whatIsGettingHurt.Value-=damage;
        }

        public static void Attack(int damage, IntReactiveProperty whatIsGettingHurt)
        {
            whatIsGettingHurt.Value-=damage;
        }

        public static void Heal(float healQuantity,FloatReactiveProperty whatIsGettingHealed)
        {
            whatIsGettingHealed.Value+=healQuantity;
        }

        public static bool IsItDead(float hp)
        {
            if (hp <= 0) return true;
            else return false;

        }
        public static bool IsItDead(int hp)
        {
            return IsItDead((float)hp);

        }

        //###########################################################################################################################
        // Time Generic  actions
        //###########################################################################################################################

        public static void OverTime(Action action, TimeSpan every, TimeSpan duration)
        {
            Observable.Interval(every)
            .Take(CalculateNumberOfTicks(every, duration))
            .Subscribe(_ =>
            {
                action();
            });
        }

        public static void OverTime(Action action, int ticks,TimeSpan every, TimeSpan duration)
        {
            
            var tickDuration = TimeSpan.FromMilliseconds(every.TotalMilliseconds / ticks);
            OverTime(action, tickDuration, duration);

        }

        //[Depracted but still in Research] Find a way without using ReactiveProperties?
        // public static void DamageOverTime(ref float affectedValue,float damage,TimeSpan every, TimeSpan duration);

        //###########################################################################################################################
        // Healing Over time actions
        //###########################################################################################################################

        public static void DamageOverTime(FloatReactiveProperty target, float totalDamage, TimeSpan every, TimeSpan duration)
        {
            var numberOfTicks = CalculateNumberOfTicks(every, duration);
            var damagePerTick = totalDamage/numberOfTicks;
            
            OverTime(() => Attack(damagePerTick,target), every, duration);
        }

        public static void DamageOverTime(FloatReactiveProperty target, float damage,int ticks, TimeSpan every, TimeSpan duration)
        {
            
            OverTime(() => Attack(damage,target),ticks, every, duration);
            
        }

        public static void DamageOverTimeImmediate(FloatReactiveProperty target, float totalDamage, TimeSpan every, TimeSpan duration)
        {
            var numberOfTicks = CalculateNumberOfTicks(every, duration);
            var damagePerTick = totalDamage / numberOfTicks;
            //
            Attack(damagePerTick,target); //Immediatly in t=0, attack the target
            DamageOverTime(target,totalDamage-damagePerTick,every,duration);
        }


        //###########################################################################################################################
        // Healing Over time actions
        //###########################################################################################################################
        public static void HealingOverTime(FloatReactiveProperty target, float totalHealing, TimeSpan every, TimeSpan duration)
        {
            var numberOfTicks =  CalculateNumberOfTicks(every,duration);
            var healingPerTick = totalHealing / numberOfTicks;
            OverTime(()=>Heal(healingPerTick,target),every,duration);
        }

        public static void HealingOverTime(FloatReactiveProperty target, float healing, int ticks, TimeSpan every, TimeSpan duration)
        {
            OverTime(() => Heal(healing/ticks, target),ticks, every, duration);
        }

        
        public static void HealingOverTimeImmediate(FloatReactiveProperty target, float totalHealing, TimeSpan every, TimeSpan duration)
        {
            var numberOfTicks = CalculateNumberOfTicks(every, duration);
            var healingPerTick = totalHealing / numberOfTicks;
            //
            Attack(healingPerTick, target); //Immediatly in t=0, attack the target
            HealingOverTime(target, totalHealing - healingPerTick, every, duration);
        }


        //###########################################################################################################################
        // Private utility  actions
        //###########################################################################################################################

        /// <summary>
        /// Use when you need to calculate how many ticks will happen during the time period.
        /// </summary>
        /// <param name="every">Every x milliseconds</param>
        /// <param name="duration">During this period of time</param>
        /// <returns>The number of ticks in that time period</returns>
        private static int CalculateNumberOfTicks(TimeSpan every, TimeSpan duration)
        {
            return (int)(duration.TotalMilliseconds / every.TotalMilliseconds);
        }
    }

}