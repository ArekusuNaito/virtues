﻿namespace Virtues
{
	using UnityEngine;
	using System;
	
	using System.Collections.Generic;
	//It is common to create a class to contain all of your
	//extension methods. This class must be static.
	public static partial class ExtensionMethods
	{
		//Even though they are used like normal methods, extension
		//methods must be declared static. Notice that the first
		//parameter has the 'this' keyword followed by a Transform
		//variable. This variable denotes which class the extension
		//method becomes a part of.
		
		/// <summary>
		/// Set only xComponent with the yComponent intact
		/// </summary>
		/// <param name="body"></param>
		/// <param name="xComponent"></param>
        public static void SetVelocityX(this Rigidbody2D body,float xComponent)
        {
            body.velocity = new Vector2(xComponent,body.velocity.y);

        }

		public static void SetVelocityY(this Rigidbody2D body,float yComponent)
        {
            body.velocity = new Vector2(body.velocity.x,yComponent);

        }

		
	}
}