﻿namespace Virtues
{
	using UnityEngine;
	using System;
	
	using System.Collections.Generic;
	//It is common to create a class to contain all of your
	//extension methods. This class must be static.
	public static partial class ExtensionMethods
	{
		//Even though they are used like normal methods, extension
		//methods must be declared static. Notice that the first
		//parameter has the 'this' keyword followed by a Transform
		//variable. This variable denotes which class the extension
		//method becomes a part of.
		public static bool HasComponent<T>(this GameObject gameObject)
		{
			return gameObject.GetComponent<T>() != null;
		}

		public static bool HasComponent<T>(this Transform gameObject)
		{
			return gameObject.GetComponent<T>() != null;
		}

		public static Vector2 GetSizeInWorldUnits(this RectTransform transform)
		{
			var corners = new Vector3[4];
			transform.GetWorldCorners(corners);
			return new Vector2(corners[3].x-corners[0].x,corners[1].y-corners[0].y);
		}

		public static bool HasComponent<T>(this MonoBehaviour gameObject)
		{
			return gameObject.GetComponent<T>() != null;
		}
		
		/// <summary>
		/// Detroys every children. ⚠ There's no rollback.
		/// </summary>
		/// <param name="transform"></param>
		public static void DestroyChildren(this Transform transform)
		{
			foreach(Transform child in transform)
			{
				UnityEngine.GameObject.Destroy(child.gameObject);
			}
		}

		

		public static Vector3 CenterUp(this Bounds bounds)
		{
			return new Vector3(bounds.center.x,bounds.max.y,bounds.center.z); 
		}
        public static Vector3 CenterLeft(this Bounds bounds)
        {
            return new Vector3(bounds.min.x, bounds.center.y, bounds.center.z);
        }
        public static Vector3 CenterDown(this Bounds bounds)
        {
            return new Vector3(bounds.center.x, bounds.min.y, bounds.center.z);
        }
        public static Vector3 CenterRight(this Bounds bounds)
        {
            return new Vector3(bounds.max.x, bounds.center.y, bounds.center.z);
        }


		//Huh... so, i guess i need to research this bitwise operations results
        //Read as: If we collide with the defined layer in this script, which would be a collision layer, then destroy it always.
		public static bool CollidesWithMask(this Collider2D me,LayerMask collisionMask)
        {
            return me.gameObject.CollidesWithMask(collisionMask);
        }
		public static bool CollidesWithMask(this Collision2D me,LayerMask collisionMask)
        {
            return me.gameObject.CollidesWithMask(collisionMask);
        }

		//Shared functionality for Collision,Collider
		public static bool CollidesWithMask(this GameObject me,LayerMask collisionMask)
		{
			return (collisionMask == (collisionMask | (1 << me.layer)));
		}



		
	}
}