﻿namespace Virtues
{
    using UnityEngine;
    using UnityEngine.Events;
    [System.Serializable]
    public class Vector2Event: UnityEvent<Vector2>
    {

    }
    [System.Serializable]
    public class TransformEvent: UnityEvent<Transform>
    {

    }
    [System.Serializable]
    public class Collider2DEvent: UnityEvent<Collider2D>
    {

    }
    [System.Serializable]
    public class Collision2DEvent: UnityEvent<Collision2D>
    {

    }
}
