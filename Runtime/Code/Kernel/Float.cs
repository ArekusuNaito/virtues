﻿namespace Virtues
{
    
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using UniRx;

[CreateAssetMenu(fileName = "New Float", menuName = "Virtues/Variables/Float", order = 1)]
public class Float : ScriptableObject
{
    // [SerializeField] float value = 0;
    [SerializeField]UniRx.FloatReactiveProperty value = new UniRx.FloatReactiveProperty(0);
    public bool resetOnEnable=true;
    // public float Value
    // {
    //     get{return this.value;}
    //     set{this.value=value;}
    // }

    
    public System.IDisposable Subscribe(IObserver<float> onSubscribe)
    {
        return this.value.Subscribe(onSubscribe);
    }

    void OnEnable()
    {
        if(resetOnEnable)this.Value = 0;
        
    }

    public void SubscribeToText(Text text, System.Func<float,string> selector)
    {
        this.value.SubscribeToText(text,selector);
    }

    public float Value
    {
        get{return this.value.Value;}
        set{this.value.Value=value;}
    }

    public FloatReactiveProperty Observable
    {
        get{return this.value;}
    }

    public void Add(float addValue)
    {
        this.value.Value+=addValue;
    }

    public static implicit operator float(Float reference)
    {
        return reference.Value;
    }
}

}