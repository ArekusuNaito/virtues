namespace Virtues
{
    using UnityEngine;
    using System;
    public static partial class Kernel
    {
        
        public static Component Create<Component>(string gameObjectName = "Unnamed GameObject")
        {

            return new GameObject(gameObjectName).AddComponent(typeof(Component)).GetComponent<Component>();
        }

        public static ClassName CloneScriptableObject<ClassName>(ScriptableObject scriptableObject) where ClassName : ScriptableObject
        {

            var cloneObject = ScriptableObject.Instantiate(scriptableObject) as ClassName;
            cloneObject.name = $"{scriptableObject.name}";
            return cloneObject;

        }
    }
    
}
