﻿ namespace Virtues
{
    using UnityEngine;
    using Web;
    using Services;
    using Audio;
    using Input;
    using Entities;
    using UI;
    using System;
    using UniRx;
    [CreateAssetMenu(fileName = "Virtues Global Settings", menuName = "PaladinEngine/Kernel/Settings", order = 1)]
    public class GlobalSettings : ScriptableObject 
    {
        [Header("General?")]
        public Font gameFont;
        [Header("SFX")]
        public AudioClip cursorMoving;
        public AudioClip submit;
        public AudioClip cancel;
        public AudioClip nextDialogue;
        public AudioClip endDialogue;
        [Header("Windows")]
        public Sprite windowSkinSprite;
        public Sprite commandCursorSprite;
        

        
    }
    
    
}


