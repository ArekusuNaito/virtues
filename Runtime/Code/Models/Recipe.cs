﻿namespace Virtues.Models
{
    using System.Collections.Generic;
    using UnityEngine;
    using System.Linq;

    
    [CreateAssetMenu(fileName = "New Recipe", menuName = "PaladinEngine/Models/New Recipe", order = 1)]
    [System.Serializable]
    public class Recipe : ScriptableObject
    {
        public List<RecipeRequirement> requirements = new List<RecipeRequirement>();
        public List<RecipeRequirement> awards = new List<RecipeRequirement>();

        public List<Item> ItemsRequired
        {
            get
            {
                return requirements.Select(requirements=>requirements.item).ToList();
            }
        }
    }

    [System.Serializable]
    public struct RecipeRequirement
    {
        public RecipeRequirement(Item item,int quantity)
        {
            this.item = item;
            this.quantity = quantity;
        }
       public Item item; 
       public int quantity;
    }

    

}