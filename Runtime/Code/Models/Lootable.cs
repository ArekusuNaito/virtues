﻿namespace Virtues.Models
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public interface Lootable
    {
        List<Drop> Drops {get; set;}
        List<Item> GenerateItems();
        
    }

    [System.Serializable]
    public struct Drop
    {
        public Drop(Item item, float dropChance)
        {
            this.item = item;
            this.dropChance=dropChance;
        }
        public Item item;
        [Range(0,100)]
        public float dropChance;
    }

}