﻿namespace Virtues.Models
{
    
    
    using UniRx;
    public interface Priceable
    {
        IntReactiveProperty Price { get; set; }
    }

    public interface Stackable
    {
        IntReactiveProperty StackSize { get; set; }
    }

    public interface Buyable : Priceable {}
    public interface Sellable: Priceable {}
    
}




