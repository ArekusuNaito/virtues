﻿namespace Virtues.Models.Save
{

    [System.Serializable]
    public class SoundOptionsData
    {
        public float masterVolume;
        public float musicVolume;
        public float sfxVolume;
        
        public SoundOptionsData(float master,float music, float sfx)
        {
            this.masterVolume = master;
            this.musicVolume = music;
            this.sfxVolume = sfx;
        }

        public override string ToString()
        {
            return $"{this.masterVolume},{this.musicVolume},{this.sfxVolume}";
        }
    }

}