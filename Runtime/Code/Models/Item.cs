﻿namespace Virtues.Models
{
    using UnityEngine;
    using System;
    using UniRx;

    [CreateAssetMenu(fileName = "New Item", menuName = "PaladinEngine/Models/New Item", order = 1)]
    [Serializable]
    public class Item : ScriptableObject,Priceable,Stackable
    {
        public IntReactiveProperty quantity = new IntReactiveProperty(0);
        [SerializeField] IntReactiveProperty sellPrice = new IntReactiveProperty(0);
        public IntReactiveProperty SellPrice
        {
            get{return this.sellPrice;}
            set{this.sellPrice = value;}
        }

        [SerializeField] IntReactiveProperty buyPrice = new IntReactiveProperty(0);
        public IntReactiveProperty Price { get => this.buyPrice; set => buyPrice = value; }
        [SerializeField] IntReactiveProperty stackSize = new IntReactiveProperty(0);
        public IntReactiveProperty StackSize { get => this.stackSize; set => this.stackSize = value; }

        
        public int HowMuchMoreCanYouStack()
        {
            return this.stackSize.Value-this.quantity.Value;
        }
    }

    // public struct SellableProperties
    // {
    //     public IntReactiveProperty sellPrice;
    //     public BoolReactiveProperty isSellable;
    // }
}
