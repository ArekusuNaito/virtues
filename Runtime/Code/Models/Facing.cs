﻿namespace Virtues
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    
    /// <summary>
    /// This is a 4 points cardinal system. Up,Down,Left,Right[]
    /// </summary>
    [System.Serializable]
    public static class CardinalPoints
    {
        [System.Serializable]
        public enum Points { Up = 0, Left = 1, Down = 2, Right = 3 };
        [System.Serializable]
        public class Direction
        {
            readonly public Vector2 vector; //closest to a constant
            readonly public Points value;
            
            public Direction(Vector2 vector,Points value)
            {
                this.vector = vector;
                this.value=value;
            }
        }

        readonly static Direction up = new Direction(Vector2.up, Points.Up); //read-only makes it closest to a constant
        readonly static Direction left = new Direction(Vector2.left, Points.Left);
        readonly static Direction down = new Direction(Vector2.down, Points.Down);
        readonly static Direction right = new Direction(Vector2.right, Points.Right);

        public static Direction Up
        {
            get{return up;}
        }
        public static Direction Left
        {
            get { return left; }
        }
        public static Direction Down
        {
            get { return down; }
        }
        
        public static Direction Right
        {
            get { return right; }
        }


        /// <summary>
        /// Vector2.SignedAngle returns angles between [-180,180), this will transform it to (0,360]
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float From180AngleTo360AngleInDegrees(float angle)
        {
            if (angle < 0)
            {
                angle *= -1;
            }
            else if (angle > 0)
            {
                angle = 360 - angle;
            }
            return angle;
        }

        /// <summary>
        /// Given an angle between 0-360 it returns the actual direction object
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Direction DegreeAngleToDirection(float angle)
        {
                if(angle>=45 && angle<=135)return Left;
                else if(angle>=135 && angle<=225)return Down;
                else if(angle>=225 && angle<=315)return Right;
                else return Up;
        }

        public static Vector3 DirectionToBoundsPosition(Direction direction,Bounds bounds)
        {
            if (direction.value==CardinalPoints.Points.Up) return bounds.CenterUp();
            else if (direction.value == CardinalPoints.Points.Left) return bounds.CenterLeft();
            else if (direction.value == CardinalPoints.Points.Down) return bounds.CenterDown();
            else return bounds.CenterRight();
        }

        

        public static Direction GetRandom4Direction()
        {
            Direction direction;
            var random = Random.Range(0,4);
            switch (random)
            {
                case 0: direction = CardinalPoints.Up; break;
                case 1: direction = CardinalPoints.Left; break;
                case 2: direction = CardinalPoints.Down; break;
                case 3: direction = CardinalPoints.Right; break;
                default: direction= CardinalPoints.Down;break;
            }
            return direction;
        }
        public static Vector2 GetRandom8Direction()
        {
            Vector2 direction;
            var random = Random.Range(0,7);
            switch (random)
            {
                case 0: direction = new Vector2(-1,1); break;
                case 1: direction = new Vector2(-1,-1); break;
                case 2: direction = new Vector2(1,-1); break;
                case 3: direction = new Vector2(1,1); break;
                case 4: direction = CardinalPoints.Up.vector; break;
                case 5: direction = CardinalPoints.Left.vector; break;
                case 6: direction = CardinalPoints.Down.vector; break;
                case 7: direction = CardinalPoints.Right.vector; break;
                default: direction= new Vector2(0,0);break;
            }
            return direction.normalized; //We normalize it, so both components will be: 0.7071068
        }

    }

}
