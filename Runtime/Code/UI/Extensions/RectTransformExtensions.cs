﻿namespace Virtues.UI
{
	/// <summary>
	/// Extensions made only for the RectTransform component
	/// </summary>

	using UnityEngine;
	using UnityEngine.UI;
	
	
	public static partial class ExtensionMethods
	{
		
		public static void SetAnchorsToFullStretch(this RectTransform transform)
		{
			transform.anchorMin = new Vector2(0,0);
			transform.anchorMax = new Vector2(1,1);
		}
		public static void SetSizeWithCurrentAnchors(this RectTransform transform,float width,float height)
		{
			transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,width);
			transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,height);
		}
	}
}