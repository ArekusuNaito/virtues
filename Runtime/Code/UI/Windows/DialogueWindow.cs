﻿

namespace Virtues.UI
{
    using System.Threading.Tasks;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using UniRx;
    using UniRx.Async;
    //Paladin Engine
    using Input;
    using Audio;
    using System.Linq;
    

    // [RequireComponent(typeof(AudioSource))]
    public class DialogueWindow : MonoBehaviour
    {
        private static DialogueWindow instance;
        [Range(1,60)]
        public int textSpeedInFrames = 12;   
        public int PlayOneShotSoundEveryXLetter = 2;
        public Image skin;
        public Text textArea;
        public RectTransform cursor;
        public AudioSource audioSource;        
        //Control variables
        public string[] dialogues;
        private string currentDialogue;
        private int currentDialogueIndex = 0;
        private int currentSymbolIndex = 0;
        public DialogueWindowSettings settings;
        public System.IObservable<long> writingState;
        // public System.IObservable<long> lastInputStream;
        private IConnectableObservable<long> lastInputStream;
        

        public void Configure()
        {

        }
        void Start()
        {
            if(instance==null)
            {
                instance = this;
            }
        }
        

        private void StartWriting()
        {
            //Sample Frame will help to increase or decrease text speed
            
            this.writingState = Observable.EveryUpdate().SampleFrame(textSpeedInFrames).Take(currentDialogue.Length);
            this.writingState.Subscribe(_ =>
            {  
                WriteSymbol();
            },WritingComplete).AddTo(this);
        }

        void WriteSymbol()
        {
            //Is the current index divisible between the number of writing times?
            //if: PlayOneShotSoundEveryXLetter is 2, means every 2 letters will PlayOneShot the sound
            if(currentSymbolIndex%PlayOneShotSoundEveryXLetter==0)this.audioSource.PlayOneShot(this.settings.letterSFX);
            currentSymbolIndex++;
            var nextText = this.currentDialogue.Substring(0, currentSymbolIndex);
            SetText(nextText);
        }

        void WritingComplete()
        {
            this.cursor.gameObject.SetActive(true);
            
            if(currentDialogueIndex>=this.dialogues.Length-1) //For the final dialogue...
            {
                this.audioSource.PlayOneShot(this.settings.endDialogueSFX);
                lastInputStream.Connect();
                lastInputStream
                .Subscribe(_=>
                {
                    this.audioSource.PlayOneShot(this.settings.submitSFX);
                },()=>this.gameObject.SetActive(false)).AddTo(this);
                
            }
            else
            {
                var waitForInput = Gamepad.CreateInputStreamDown(() => Input.GetKeyDown(KeyCode.Space));
                waitForInput.Subscribe(_ =>
                  {
                      this.audioSource.PlayOneShot(this.settings.nextDialogueSFX);
                      PrepareNextDialogue();
                  },StartWriting).AddTo(this);
            }
            
        }


        void PrepareNextDialogue()
        {
            this.cursor.gameObject.SetActive(false);
            currentDialogueIndex++; //Move to the next dialogue
            this.currentDialogue = this.dialogues[currentDialogueIndex]; //Assign the new dialogue
            this.currentSymbolIndex = 0; //Reset the symbol index to the very beggining
        }
        
        /// <summary>
        /// Completely reset the dialogue box
        /// </summary>
        void Reset()
        {
            this.cursor.gameObject.SetActive(false);
            currentDialogue = this.dialogues[0];
            this.currentSymbolIndex=0;
            this.currentDialogueIndex=0;
            this.SetText("");
        }

        // public void Show(string[] dialogues)
        // {
        //     this.dialogues = dialogues;
        // }

        //UniTask is a lightweight version of it
        public async static UniTask Show(string[] dialogues)
        {
            if(dialogues.Length==0)
            {
                Debug.LogWarning("DialogueBox can't work without any dialogues");
                dialogues = new string[]{" "};
            }
            //Is there any DialogueBox in the scene already?
            if(instance==null)//First time
            {
                instance = Resources.FindObjectsOfTypeAll<DialogueWindow>().FirstOrDefault();
                
            }
            if(instance)
            {
                
                instance.gameObject.SetActive(true);
                instance.dialogues = dialogues;
                instance.lastInputStream = Observable.EveryUpdate().Where(_=>Input.GetKeyDown(KeyCode.Space)).Take(1).Publish();
                instance.Reset();
                instance.StartWriting();
                await instance.lastInputStream.ToAwaitableEnumerator();
            }
            else
            {
                Debug.LogError("There's no DialogueWindow in Scene, Add one to the Hierarchy");
            }
        }
        

        public void SetText(string newText)
        {
            this.textArea.text=newText;
        }
    }

}
