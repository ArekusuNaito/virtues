﻿namespace Virtues.UI
{
	
	using UnityEngine;
	using UnityEngine.UI;

	/// <summary>
	/// Planned to be the base class for any window in game.
	/// </summary>
	[RequireComponent(typeof(RectTransform))]
	[RequireComponent(typeof(CanvasRenderer))]
	public class Window : MonoBehaviour 
	{	
		[Header("Graphic Customization")]
		public Image skin;
		[Header("Positioning")]
		[Header("X")]
		[Range(0,100)]
		public int widthInPercent=20;
		[Range(0,100)]
		public int x=50;
		[Range(0,100)]
		[Header("Y")]
		public int heightInPercent=40;
		[Range(0,100)]
		public int y=50;
		protected RectTransform rectTransform;
		protected RectTransform parent;

		
		protected virtual void Awake()
		{
			this.rectTransform = GetComponent<RectTransform>();
			this.parent = (RectTransform)this.rectTransform.parent;
			this.rectTransform.SetAnchorsToFullStretch();
			this.rectTransform.pivot = new Vector2(0,1);
		}

		protected virtual void Start()
		{
			this.UpdateSizeAndPosition();
		}

		protected virtual void UpdateSizeAndPosition()
		{
			
			this.rectTransform.SetSizeWithCurrentAnchors(ParentWidth/100*widthInPercent,ParentHeight/100*heightInPercent);
			this.rectTransform.anchoredPosition = new Vector2(ParentWidth/100*x,-ParentHeight/100*y); 
			
		}

		protected float Width
		{
			get{return this.rectTransform.rect.width;}
		}
		protected float Height
		{
			get{return this.rectTransform.rect.height;}
		}

		protected float ParentWidth
		{
			get{return this.parent.rect.width;}
		}
		protected float ParentHeight
		{
			get{return this.parent.rect.height;}
		}
	}


	
}
