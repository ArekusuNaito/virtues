﻿namespace Virtues.Systems
{
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;
    using Models.Save;

    [CreateAssetMenu(fileName = "New Save System", menuName = "Virtues/Systems/Save System")]
    public class SaveSystem : ScriptableObject
    {
        string SoundDataPath 
        {
            get{return $"{Application.persistentDataPath}/soundSettings.aud";}
        } 
        
        public void SaveSoundData(float master,float music,float sfx)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            // Debug.Log(soundDataPath);
            FileStream fileStream = new FileStream(SoundDataPath,FileMode.Create);
            SoundOptionsData soundOptions = new SoundOptionsData(master,music,sfx);
            formatter.Serialize(fileStream,soundOptions);
            fileStream.Close();
        }

        public SoundOptionsData LoadSoundData()
        {
            if(File.Exists(SoundDataPath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream fileStream = new FileStream(SoundDataPath,FileMode.Open);
                var soundOptionsData = formatter.Deserialize(fileStream) as SoundOptionsData;
                return soundOptionsData;
            }
            else
            {
                return null;
            }
        }
    }

}