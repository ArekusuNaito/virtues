﻿namespace Virtues.Input
{
	using UnityEngine;
	using UniRx;
	using Entities;
	using System.Dynamic;
	using System;
	/// <summary>
	/// A simple wrapper that references the UnityEngine.Input class
	/// </summary>
	public class Gamepad
	{
		public static CompositeDisposable actionStreamDisposer = new CompositeDisposable();
		private static CompositeDisposable verticalStreamDisposer = new CompositeDisposable();
		private static CompositeDisposable horizontalStreamDisposer = new CompositeDisposable();

		public static void RemoveAllInput()
		{
			actionStreamDisposer.Clear();
			verticalStreamDisposer.Clear();
			horizontalStreamDisposer.Clear();
		}

		private static System.IObservable<long> CreateButtonStream(Func<bool> button)
		{
			return Observable.EveryUpdate()
			 .Where(_=>button());
		}

		public static System.IObservable<long> CreateInputStreamDown(Func<bool> inputFunction)
		{
            return Observable.EveryUpdate()
             .Where(_ => inputFunction()).Take(1);
		}

		//################## Action Button Streams ############################


		public void AddActionButtonStream(System.Action subscribingAction)
		{
			actionStreamDisposer.Clear();
			var actionStream = CreateButtonStream(()=>ActionDown);
			actionStream.Subscribe(_=>subscribingAction()).AddTo(actionStreamDisposer);
		}

		public void AddActionButtonStream(Func<bool> conditions,System.Action subscribingAction)
		{
			actionStreamDisposer.Clear(); //There can be only one action input per button
			var actionStream = CreateButtonStream(()=>ActionDown);
			actionStream = actionStream.Where(_=>conditions());
			actionStream.Subscribe(_=>subscribingAction()).AddTo(actionStreamDisposer);
		}

		public void RemoveActionButtonStream()
		{
			actionStreamDisposer.Clear();
		}

		//################## Horizontal Button Streams ############################


		public static void AddHorizontalStream(Action<float> subscribingAction)
		{
			horizontalStreamDisposer.Clear();
			Observable.EveryUpdate().Subscribe(_=>subscribingAction(HorizontalValue)).AddTo(horizontalStreamDisposer);
			
		}

		//################## Vertical Button Streams ############################


		public static void AddVerticalStream(Action<float> subscribingAction)
		{
			verticalStreamDisposer.Clear();
			var vertical = CreateButtonStream(()=>Vertical);
			vertical.Subscribe(_=>subscribingAction(VerticalValue)).AddTo(verticalStreamDisposer);
			
		}

		//################## Button Accessors ############################


		private static float HorizontalValue
		{
			get{return Input.GetAxisRaw("Horizontal");}
		}
		public static bool Horizontal
		{
			get{return Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0; }
		}

		public static Vector2 LeftStick
		{
			get{return new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")); }
		}

		private static float VerticalValue
		{
			get{return Input.GetAxisRaw("Vertical");}
		}

		public static bool Vertical
		{
			get{return Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0; }
		}

		public static bool Right
		{
			get{return (Input.GetAxisRaw("Horizontal")) > 0; }
		}

		public static bool Left
		{
			get{return (Input.GetAxisRaw("Horizontal")) < 0; }
		}

		public static bool Up
		{
			get{return (Input.GetAxisRaw("Vertical")) > 0; }
		}

		public static bool Down
		{
			get{return (Input.GetAxisRaw("Vertical")) < 0; }
		}

		public static bool ActionDown
		{
			get{return Input.GetButtonDown("Submit");}
		}
		
	}
}