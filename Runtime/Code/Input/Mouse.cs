﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Virtues.Input
{
public class Mouse
    {
        /// <summary>
        /// Returns a vector3 with the position of the mouse in transform coordinates (NOT SCREEN POSITION PIXELS)
        /// </summary>
        /// <returns></returns>
        public static Vector3 ScreenToWorldPosition(int zDepth=10)
        {
            //From pixels to a Vector3 transform position in world coordinates
            var pixelPosition = new Vector3(UnityEngine.Input.mousePosition.x, UnityEngine.Input.mousePosition.y);
            pixelPosition.z = zDepth;
            return Camera.main.ScreenToWorldPoint(pixelPosition);
        }
        
    }

}

