# Virtues Framework


# Changelog

The motivation for these updates are to keep up the good work on my declarative programmin, readable code. These updates are providing ways to decouple code and use the built-in stuff in Unity to then create interfaces to make a more efficient workflow in Unity. This is going into a Data-Driven-Development.

## Editor Folder
    - Changed: Package "asmdef" is defined here.
    - Added: Float Editor Script for the "Float" Observable Class
## Runtime
### Core
    - Added a Float Scriptable Object. Imagine having a float variable but as an Object; plus it's an Observable. Very handy. You can interact with this object using UnityEvents.
    - Added: InspectorEvents.
    - Added: RunTimeInitializer. A static class that uses the Unity Scene Initializer methods. It's not currently being used but it can be handy as this seems to be a Unity thing.
    - Removed: Global Settings. For now I'm removing the Global Settings until I find them a better use in a Non-Inspector workflow. They were really useful in a Script-architecture. But let's remember that using the Inspector and going the Data Driven way is what I'm aiming for.
### Models
    - SaveFile
        - Added: A SoundOptionsData class to hold the values on the SoundOptions Menus.
    - Facing
        - Added: RandomDirectionWith 8 Directions. As a side note. Have to add the diagonal directions to the facing class. 
    - Properties
        - Added: Priceable. Father interface for Buyable and Sellable.
        - Added: Sellable. Use it to tag and have a SellableProperty.
        - Added: Buyable. Use it to tag and have a BuyableProperty.
    - Item
        - Changed: Item is now Priceable. Not a Sellable or Buyable for now. But it is something that just has a price
### Scene
    - Added: SceneChanger. A `ScriptableObject` to use for an Async SceneLoad. Just need one of these. And the functions will be used everywhere.
### Audio
    - Added: AudioCollection. A Scriptable Object. that holds a list of AudioClips.
    - Removed: AudioChannels class. Too code-coupled. Experimentation has showed me that UnityEvents are a good path to go. As I'm intending to remove coupling.
    - Removed: AudioMixer. Same reason. I even used the Unity built-in Audio mix.
    - Added: AudioPlayer ScriptableObject. This unique object will be the one that will Route the sounds and that every other UnityEvent can use from the inspector.
### Services
    - Inventory
        - Proper changes with the `Priceable` property.
    - Time
        - Added a OnEveryDeltaTime
### Systems
    - Added: SaveSystem. A `ScriptableObject` to just use the Load and Save functions. For now there's only SoundOptions, and this has to be modified as the Framework keeps getting different data types.
### Extension Methods
    - Added: New Extension methods such as
        - Get how much size a RectTrasform occupies in World Units.
        - CollidesWithMask(). You can ask a game object,collider2D,collision2D if collided with a given mask.
        - Added a SetVelocityX() and SetVelocityY() to the rigidbody2D objects. If you just want to set a specific vector component, you can use these. The other component will keep the current value.
## Prefabs
    - Added: A Simple Unity Audio Mixer. It has 3 Channels.
        - A Master Channel
            - A Music Channel
            - An SFX Channel.
## Plugins
#### UniRx
    - Changed: PlayerLoopHelper so it won't use the Experimental unity package.