This package contains third-party software components governed by the license(s) indicated below:

Component Name: UniRx

License Type: "MIT"

[SemVer License](https://github.com/neuecc/UniRx/blob/master/LICENSE)

Component Name: DOTween

License Type: "MIT"

[DOTween License](http://dotween.demigiant.com/license.php)