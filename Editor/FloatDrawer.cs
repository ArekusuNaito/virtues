﻿
namespace Virtues.Editor
{
    using UnityEditor;
    using UnityEngine;
    

    [CustomPropertyDrawer(typeof(Float))]
    public class FloatDrawer : PropertyDrawer
    {
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            if(property.objectReferenceValue!=null)
            {
                SerializedObject scriptableObjectSerialized = new SerializedObject(property.objectReferenceValue);
                var valueProperty = scriptableObjectSerialized.FindProperty("value");
                
                
                position = EditorGUI.PrefixLabel(position, label);
                
                    var scriptableObjectRect = new Rect(position.x,position.y,position.width/2,position.height);
                    var valueRect = new Rect(position.x+position.width/2,position.y,position.width/2,position.height);
                    //
                    
                    EditorGUI.PropertyField(scriptableObjectRect,property,GUIContent.none);
                    EditorGUI.PropertyField(valueRect,valueProperty,GUIContent.none);
                        
                    if(EditorGUI.EndChangeCheck())
                    {
                        scriptableObjectSerialized.ApplyModifiedProperties();
                        property.serializedObject.ApplyModifiedProperties(); //I don't understand why this is not needed, I thought this needs would apply changes to the scriptable object change
                    }
                
            }
            else
            {
                var defaultRect = new Rect(position.x,position.y,position.width,position.height);
                EditorGUI.ObjectField(defaultRect,property);
            }
            
            EditorGUI.EndProperty();
            
        }
    }

}